$(function(){
    chrome.storage.sync.get(['total','limit'],function(budget){
        $('#total').text(budget.total);
        $('#limit').text(budget.limit);
    });
    $('#add').click(function(){
        chrome.storage.sync.get('total',function(budget){
            var totalAmount = 0;
            if(budget.total){
                totalAmount = parseFloat(budget.total);
            }
            var amount = $('#amount').val();
            if(amount){
                totalAmount += parseFloat(amount);
                chrome.storage.sync.set({'total':totalAmount})
            }
            $('#total').text(totalAmount);
            $('#amount').val('');
        })
    })
})